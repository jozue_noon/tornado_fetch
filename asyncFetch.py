#!/usr/bin/python3
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
#import threading
#import functools

tornado.httpclient.AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")


def callbackfun(response):

    if response.error:
        print("Error:", response.error)
    else:
        body = str(response.body)
        print(body[0:100])


class PeriodicFetcher:
    """
    Class to fetch list of urls.

    Usage:
    * create object instance and pass list of urls as constructor argument
    * start fetching by running instance.run() if you are ready
    """

    def __init__(self, url_list = []):
        if not isinstance(url_list, list):
            raise TypeError("Argument not a list: {0}".format(url_list))
        self.urls_to_fetch = url_list
        self.http_client = tornado.httpclient.AsyncHTTPClient()



    def handle_request(self, orig_callback):
        """
        General response handler.
        :param response:
        :return:
        """
        def wrapper(response):
            # Do stuff before.
            orig_callback(response)

        return wrapper

    def run(self, callback):
        """
        Spawns fetch jobs and runs IOLoop instance.
        :return:
        """
        # Just decoration :)
        callback = self.handle_request(callback)

        for url in self.urls_to_fetch:
            self.http_client.fetch(url, callback)



class BasicFetcher(PeriodicFetcher):

    def __init__(self, url_list):
        super().__init__(url_list)
        self.remaining_urls = len(url_list)
        self.ioloop = tornado.ioloop.IOLoop.instance()

    def run(self, callback):
        super().run(callback)
        self.ioloop.start()

    def handle_request(self, orig_callback):
        """
        General response handler.
        :param response:
        :return:
        """
        def wrapper(response):

            self.remaining_urls -= 1

            orig_callback(response)

            if self.remaining_urls == 0:
                self.http_client.close()
                self.ioloop.stop()

        return wrapper



if __name__ == "__main__":

    import time
    beginTime = time.time()
    test_list = ["https://www.serverdensity.com/",
                 "www.onet.pl",
                 "www.gazeta.pl"]

    bFetcher = BasicFetcher(test_list)
    bFetcher.run(callbackfun)
    print("=======================================")
    print(time.time() - beginTime)