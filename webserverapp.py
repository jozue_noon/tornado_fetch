
import tornado.web
import tornado.websocket
import logging
from threading import Lock
import copy

class StatusHandler(tornado.web.RequestHandler):

    def initialize(self, data, checkInterval):
        self._data = data
        self._checkInterval = checkInterval

    @tornado.web.asynchronous
    def get(self):
            self.render("status.html",
                        statusdata = self._data,
                        checkInterval = self._checkInterval)


# TODO - impement websocket for direct data streaming.
# class WebSocketStatusHandler(tornado.websocket.WebSocketHandler):
#
#     clients = []
#
#     def __init__(self):
#         self.log = logging.getLogger("WebSocket")
#
#     def open(self, *args):
#         self.log.info("Status web socket opened.")
#         WebSocketStatusHandler.clients.append(self)


