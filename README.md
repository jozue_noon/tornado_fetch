# WebMonitor

WebMonitor is simple tornado based application for monitoring websites. Url fetching is scheduled
with PeriodicCallback. Same ioloop instance is serving simple http page with current status information.

## Requirements

Make sure you have installed ```tornado``` if there is ```pip``` available in your system type following in console:
```bash
$ sudo pip install tornado
```

## Quick start

Clone project to your system or download zip file:
```bash
$ git clone git@bitbucket.org:jozue_noon/tornado_fetch.git
```

To run WebMonitor type following in console:
```bash
$ cd /path/to/cloned/repo
$ chmod +x web_monitor.py
$ ./web_monitor.py monitor.conf &
```

To conveniently watch log file type following:
```bash
$ tail -f web_monitor.log
```

Simple web page interface is located under following url:
[http://127.0.0.1:8000/status](http://127.0.0.1:8000/status)



If you use ```&``` with WebMonitor application is going to background.
You could kill process with these two methods:
```bash
$ jobs
[1]+  Running                 ./web_monitor.py monitor.conf &
$ fg 1
^C
```

Or simply:
```bash
$ jobs
[1]+  Running                 ./web_monitor.py monitor.conf &
$ kill %1
```




## Configuration file
To run script you have to supply proper configuration file. See example ```monitor.conf```.

```config
[global]
# Global configuration
connectionTimeout = 1
requestTimeout = 1
checkInterval = 5

#############################
# Urls to fetch configuration

[url.onet]
url = www.onet.pl
lookUpContent = Onet
```

1. ```checkInterval``` is checking time interval in seconds,
1. ```lookUpContent``` is new line delimited content of web page to serch for.
Given string is stripped of preceding and trailing white characters.

## Log file

Log file is stored in ```web_monitor.log``` file.

Example site report:
```log
 === Report for url : www.nasa.gov ===
        Errors if any: None
        Status code: 200
        Matches: 4/4
        Request time: 0.042591094970703125"
```

Report consist of:

1. Errors information,
1. Response status code,
1. Matches - how many requirements were met,
1. Request time in seconds.

Intervals are separated with ```== SEPARATOR ==```



## Running tests

One could run test with:
```bash
$ python3 -m tornado.testing tests
```
Note that there is very poor test coverage for now.

### TODO

* In test if we exceed 5 seconds the test timeouts. If this is fine could stay but if not some workaround is needed.
* IOLoop instance in tests.py have both client and server to handle so there is always a little uncertainty but
test clearly indicates that there is non linear relationship between number of fetched urls and total time
needed for code to run. Could be fixed later...
* Responser could be modified to accept fractional delay times.
* From BasicFetcher remove shouting handle_request behaviour.