#!/usr/bin/python3

from responser import Responser
from asyncFetch import BasicFetcher, callbackfun

from tornado.testing import AsyncHTTPTestCase

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
import time
from time import sleep
from tornado.httputil import url_concat
from unittest.mock import MagicMock, call



class TestAscyncFetch(AsyncHTTPTestCase):

    def setUp(self):
        super(TestAscyncFetch,self).setUp()

        port = self.get_http_port()
        time_1 = {'waittime': 1}
        time_2 = {'waittime': 2}
        time_3 = {'waittime': 3}

        self.max_time = 3

        self.test_list = [url_concat("http://127.0.0.1:{0}".format(port), time_1),
                     url_concat("http://127.0.0.1:{0}".format(port), time_1),
                     url_concat("http://127.0.0.1:{0}".format(port), time_1),
                     url_concat("http://127.0.0.1:{0}".format(port), time_1),
                     url_concat("http://127.0.0.1:{0}".format(port), time_1),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_2),
                     url_concat("http://bad_url", time_2),
                     url_concat("http://127.0.0.1:{0}".format(port), time_3)]

    def get_app(self):
        # Create server here, we have to ask somewhere.

        app = tornado.web.Application(handlers=[(r"/", Responser)])
        return app

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.current()


    def test_if_server_returns_something(self):
        arguments = {'waittime': 2}
        url = url_concat("/", arguments)
        response = self.fetch(url)
        self.assertEqual(response.code, 200)
        prepStr = "{0} seconds!".format(arguments['waittime'])
        self.assertIn(prepStr, str(response.body))

    def test_if_server_is_in_async_mode(self):
        pass


    def test_basic_fetcher_with_mock_callback(self):
        time_1 = {'waittime': 1}
        small_list = [url_concat("http://127.0.0.1:{0}".format(self.get_http_port()), time_1)]

        mhandle_request = MagicMock()
        bFetcher = BasicFetcher(small_list)
        bFetcher.run(mhandle_request)
        print(mhandle_request.mock_calls)
        # mhandle_request.assert_called_with([call(HTTResponse())])

    def test_basic_fetcher(self):

        beginTime = time.time()
        bFetcher = BasicFetcher(self.test_list)
        bFetcher.run(callbackfun)
        timeDiff = time.time()-beginTime
        print("=======================================")
        print("Elspsed time should be less than 3-5 seconds: {0}".format(timeDiff))
        self.assertLess(timeDiff, self.max_time + 2.0)



