#!/usr/bin/python3 -OO

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import time

from tornado.options import define, options
define("port", default=8000, help="Rabbit hole...", type=int)


class Responser(tornado.web.RequestHandler):
    """
    Server class that is simulator for long response server.
    App accepts /?waittime={time_in_seconds} parameter but not longer than 4 seconds due to
    tests time out.
    Simulates behavior like long IO.
    """
    @tornado.web.asynchronous
    def get(self):
        # Random time...
        # waitTime = randint(1,10)
        # But even better send via argument ! :)
        mytime = self.get_argument('waittime', '1')
        defaultwaittime = 1
        try:
            to_int_wait = abs(int(mytime))
            if to_int_wait > 4:
                to_int_wait = defaultwaittime
        except:
            to_int_wait = defaultwaittime

        res = 'Was waiting for: {0} seconds!'.format(mytime)
        self.write(res)
        tornado.ioloop.IOLoop.instance().add_timeout(time.time() + to_int_wait, self._sleep_process)

    def _sleep_process(self):
        self.write("<br>Time end.")
        self.finish()


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", Responser)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


