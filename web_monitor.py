#!/usr/bin/python3

import logging
import logging.handlers
import os
import sys
import configparser
from time import sleep
import tornado
import io
from tornado.httpclient import HTTPRequest
import tornado.httpserver
from asyncFetch import BasicFetcher, PeriodicFetcher
import copy

from webserverapp import StatusHandler

from tornado.options import define, options
define("port", default=8000, help="Rabbit hole...", type=int)

LOGFORMAT = '%(asctime)s [%(process)d/%(threadName)-10s] %(levelname)-8s "%(module)s/%(funcName)s:%(lineno)d" "%(message)s"'


def setlogging():

        logHandler = logging.handlers.RotatingFileHandler('web_monitor.log', maxBytes = 5*1024*1024, backupCount = 3)
        logHandler.setFormatter(logging.Formatter(LOGFORMAT))
        logging.getLogger( ).addHandler(logHandler)
        logging.getLogger( ).setLevel(logging.INFO)
##

# -------------------------------------------------##


class WebMonitor():

    def __init__(self, confPath):
        self.setlogging()
        try:
            self.cfg = configparser.ConfigParser()
            self.cfg.read_string(self.getdefaults())
            self.cfg.read(confPath)

        except:
            self.logging.exception("Configuration processing error:")
            sys.exit(1)
        ##

        self.urls = {}
        self.loadurlsfromconfig()

        self.ioloop = None
        self.http_client = None

        self.requests = []

        self.prep_data = []
        self.current_data = []

        connTimeout = float(self.cfg.get("global", "connectionTimeout"))
        reqTimeout = float(self.cfg.get("global", "requestTimeout"))
        self.checkInterval = int(self.cfg.get("global","checkInterval"))

        for url, requirements in self.urls.items():
            self.requests.append(HTTPRequest(url,
                                        method='GET',
                                        connect_timeout=connTimeout,
                                        request_timeout=reqTimeout))
        ##
    ##

    def setlogging(self):
        self.logging = logging.getLogger('WebMonitor')

    def getdefaults(self):
        return\
        """
        [global]\n
        connectionTimeout = 1\n
        requestTimeout = 1\n
        checkInterval = 30\n

        #[url.nasa]\n
        #url = www.nasa.gov\n
        #lookUpContent = NASA\n
        """

    def loadurlsfromconfig(self):

        for section in self.cfg.sections():
            if section.split('.')[0] != 'url':
                continue
            url = self.cfg.get(section, 'url')
            content = self.cfg.get(section, 'lookUpContent').splitlines()
            content = [val.strip() for val in content]
            self.urls[url] = content
        self.logging.info("Loaded {0} urls from config file.".format(len(self.urls)))

    def loggingcallback(self,response):

        vars = {}
        vars['matched'], vars['of_requirements'] = self.requirements_matched(response.request.url, str(response.body))
        vars['url'] = response.request.url
        vars['status'] = response.code
        vars['reqtime'] = response.request_time
        vars['error'] = response.error
        self.logging.info(self.report(vars))
        self.prep_data.append(vars)
        if len(self.prep_data) == len(self.requests):
            self.current_data[:] = self.prep_data[:]

    def run(self):

        while True:
            try:
                bFetcher = BasicFetcher(self.requests)
                bFetcher.run(self.loggingcallback)
                logging.info('\n\n ============================================= SEPARATOR =============================================\n')
                sleep(self.checkInterval)
            except KeyboardInterrupt:
                self.logging.info("Key interrupt. Exiting.")
                break

    def periodiccallback(self):
        logging.info('\n\n ============================================= SEPARATOR =============================================\n')
        self.prep_data = []
        for url in self.requests:
            self.http_client.fetch(url, self.loggingcallback)

    def run_periodic(self):

        self.ioloop = tornado.ioloop.IOLoop.instance()
        self.http_client = tornado.httpclient.AsyncHTTPClient()

        tornado.ioloop.PeriodicCallback(self.periodiccallback,self.checkInterval*1000).start()

        app = tornado.web.Application([(r'/status', StatusHandler, {'data': self.current_data, 'checkInterval': self.checkInterval})])
        http_server = tornado.httpserver.HTTPServer(app)
        http_server.listen(options.port)

        self.ioloop.start()



    def requirements_matched(self, url, body):
        matched = 0
        requirements = 0
        req_vals = self.urls.get(url, None)

        if req_vals is None:
            return matched, requirements
        requirements = len(req_vals)

        for reqirement in req_vals:
            if body.find(reqirement) >= 0:
                matched += 1
        return matched, requirements




    def report(self, vars):

        report_str = """
        === Report for url : {url} ===
        Errors if any: {error}
        Status code: {status}
        Matches: {matched}/{of_requirements}
        Request time: {reqtime}""".format(**vars)

        return report_str



# -------------------------------------------------##


def initialize():
    setlogging()


def usage():
    print("Call this script with proper configuration file with 'conf' extension <filename>.conf")



if __name__ == '__main__':

    initialize()
    input_args = sys.argv[1:]

    if len(input_args) != 1:
        usage()
        logging.error("Bad number of input arguments. Exiting.")
        sys.exit(404)
    else:
        #Check format
        confPath = input_args[0]
        conf_name = os.path.basename(input_args[0])
        check_conf = conf_name.split('.')[1]
        if check_conf != 'conf' or not os.path.isfile(confPath):
            usage()
            logging.error("Not a config file! Exiting.")
            sys.exit(403)


    runner = WebMonitor(confPath)
    runner.run_periodic()
    #runner.run()